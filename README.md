# Microservice architectures: Automated HTTP headers propagation

<h3>Tools:</h3>
* Java 8
* Maven
* Docker

<h3>Starting the microservices:</h3>
1. `mvn clean package docker:build`
2. `docker-compose -f docker/common/docker-compose.yml up`

Make sure the `run.sh` files in the `src/main/docker` folders have `LF` line endings when creating the docker images (meant to be run in linux containers).
